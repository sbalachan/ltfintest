echo "Generating fixed width file /apps/data/output.txt"
sleep 1
python3 GenerateFixedWidthFile.py -s spec.json
sleep 1
echo "Generating csv file /apps/data/output.csv"
sleep 1
python3 ParseAndGenerateCSV.py -s spec.json
while [ 1 ]
  do
      sleep 2
      if [[ -f /apps/data/output.txt && -f /apps/data/output.csv ]]; then
        echo "Files generated in directory /apps/data. Use docker volume command to connect to the directory and collect the data"
      fi

  done
