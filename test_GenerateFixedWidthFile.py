from GenerateFixedWidthFile import generate_string, generate_row

def test_generate_string_1():
    assert len(generate_string(1))==1

def test_generate_string_2():
    assert len(generate_string(0))==0

def test_generate_row_1():
    assert len(generate_row('%2s%2s',[2,2]))==4
