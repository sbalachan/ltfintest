## Enter docker image name as an argument.
pytest > testlogs.txt
cat testlogs.txt
!(grep failed testlogs.txt) && echo "!!!! Success. Proceeding to Docker building. Please see $(PWD)/testlogs.txt for test results." && docker build -t $1 .
(grep failed testlogs.txt) && echo "Failed some tests. Please see $(PWD)/testlogs.txt for test results."
