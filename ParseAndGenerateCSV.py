import struct
import json


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--SpecificationFile', help= "Parses a fixed width file based on specification and converts it into a csv file.")
    args = parser.parse_args()


    if args.SpecificationFile:

        try:


            with open(args.SpecificationFile,'r') as spec_file_handle:
                spec_raw_string = spec_file_handle.read()

            spec_dict = json.loads(spec_raw_string)
            column_widths = tuple(map(lambda x: int(x), spec_dict['Offsets']))
            # Make a string that represents the structure of a row in the fixed width file.
            # This is to extract or parse data from the fixed width file.
            fmt_string = ' '.join(map(lambda x:f'{x}s', column_widths))

            # Create a function that Parses data from a single row of fixed width file.
            unpack = struct.Struct(fmt_string).unpack_from
            parse = lambda line: tuple(s.decode() for s in unpack(line.encode()))

            # Read from fixed width file('Output.txt') and write to a csv file('output.csv')
            with open('./data/output.txt','r', encoding=spec_dict['FixedWidthEncoding']) as input_file_handle:
                with open('./data/output.csv','w', encoding=spec_dict['DelimitedEncoding']) as output_file_handle:

                    lines = input_file_handle.readlines()

                    for line in lines:
                        fields = parse(line) # Parsing happenes here.
                        fields = tuple(map(lambda x: x.lstrip(), fields))
                        row = f'{",".join(fields)}\n'
                        output_file_handle.write(row)

        except Exception as e:
            print(f'Error:{e}')

    else:
        print("Please input specification file.")
        print("python ParseAndGenerteCSV.py -s 'location of specification file(must be a json file)'")
