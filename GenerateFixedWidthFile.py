import string
import random
import json
from typing import List

def generate_string(length:int) -> str:
    '''
    This function takes an legth as argument and generates a random string
    lenght long.

    @param lenght: length of the resultant string.
    @type lenght: int
    @return: A random string lenght long.
    @rtype: str
    '''
    char = string.ascii_uppercase + string.digits + string.ascii_lowercase #+string.punctuation
    return ''.join(random.choice(char) for x in range(length))

def generate_row(fmt_string:str, column_widths:List[int]) -> str:
    '''
    This function takes 2 arguments fmt_string and column_widths. It then
    generates a string that represents a row of a fixed width file.

    @param fmt_string: A string representing a format of a row of a fixed width file. (eg. '%5s%12s%3s%2s%13s%7s%10s%13s%20s%13s')
    @type fmt_string: str
    @param column_widths: Represent the length of each field of a fixed width file.
    @type column_widths: List[int]
    @return: A string that represents a row of a fixed width file.
    @rtype: str
    '''

    row_values = tuple(map(lambda x:generate_string(x-1), column_widths))
    return fmt_string%row_values

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--SpecificationFile', help= "Generates a fixed width file as per the specification file. The specification file must be in json format.")
    args = parser.parse_args()

    if args.SpecificationFile:

        try:
            # Read the specification file which is in json format.
            with open(args.SpecificationFile,'r') as spec_file_handle:
                spec_raw_string = spec_file_handle.read()


            spec_dict = json.loads(spec_raw_string)
            column_widths = list(map(lambda x: int(x), spec_dict['Offsets']))
            # Make the format string that represents the structure of the row of a fixed width file.
            # (eg. '%5s%12s%3s%2s%13s%7s%10s%13s%20s%13s')
            fmt_string = "".join(map(lambda x:f'%{x}s', column_widths))

            # Generate random data and write it to output.txt file. 200 rows.
            with open('./data/output.txt','w', encoding=spec_dict['FixedWidthEncoding']) as output_file_handle:

                header = fmt_string%tuple(spec_dict['ColumnNames'])
                output_file_handle.write(f'{header}\n')

                # Generate and write the data. 200 rows.
                for _ in range(200):
                    row = generate_row(fmt_string,column_widths)
                    output_file_handle.write(f'{row}\n')

        except Exception as e:
            print(f'Error:{e}')


    else:
        print("Please specify specification file.")
        print("python GenerateFixedWidthFile.py -s 'location of specification file (must be a json file)'")
