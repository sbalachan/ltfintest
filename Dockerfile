FROM ubuntu:18.04

RUN apt-get update &&\
    apt-get install python3-pip -y
RUN python3.6 -m pip install --upgrade pip
RUN pip3 install -U pytest

WORKDIR /apps
COPY . /apps/
RUN chmod +x /apps/masterscript.sh

ENTRYPOINT ["/bin/bash", "masterscript.sh"]

